.LOG

## Learn C2000 Launchpad

### Working list

[详细的链接请此参考wiki](https://bitbucket.org/bpig/learnc2000/wiki/Home)

* 使用TI给的头文件建立工程，不使用driverlib.lib的API
* ...

### Log

#### 10:35 2014/12/18 使用v192创建工程

1. 建立CCS Project，选择c2000，STM32F28027，建立工程with main.c
2. 新增文件夹 inc和src，用来存放头文件和C源文件
3. 把...\f2802x\v129\DSP2802x_headers 下include和source文件分别拷入inc和src
4. 把...\f2802x\v129\DSP2802x_common 下的include和source文件分别拷入inc和src
5. 把...\f2802x\v129\DSP2802x_headers\cmd 下的DSP2802x_Headers_nonBIOS.cmd拷入工程
6. 在CCS中添加包含路径，Right click on project, Build Properties, Include Path。
一共添加三个
...\Workspace\learnC2000,
...\Workspace\learnC2000\inc,
...\Workspace\learnC2000\src
7. 设置DSP2802x_SWPrioritizedDefaultIsr.c,DSP2802x_SWPrioritizedPieVect.c exclude from build
8. 编译...有两个警告，将DSP2802x_CSMPasswords.asm exclude from build，
程序加密代码的出错，这个只能在FLASH模式下才能工作
9. 编译，没有错误，搞定！

11:09 2014/12/19

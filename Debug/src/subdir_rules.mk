################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
src/DSP2802x_Adc.obj: ../src/DSP2802x_Adc.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_Adc.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_CodeStartBranch.obj: ../src/DSP2802x_CodeStartBranch.asm $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_CodeStartBranch.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_Comp.obj: ../src/DSP2802x_Comp.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_Comp.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_CpuTimers.obj: ../src/DSP2802x_CpuTimers.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_CpuTimers.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_DBGIER.obj: ../src/DSP2802x_DBGIER.asm $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_DBGIER.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_DefaultIsr.obj: ../src/DSP2802x_DefaultIsr.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_DefaultIsr.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_DisInt.obj: ../src/DSP2802x_DisInt.asm $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_DisInt.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_ECap.obj: ../src/DSP2802x_ECap.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_ECap.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_EPwm.obj: ../src/DSP2802x_EPwm.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_EPwm.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_GlobalVariableDefs.obj: ../src/DSP2802x_GlobalVariableDefs.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_GlobalVariableDefs.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_Gpio.obj: ../src/DSP2802x_Gpio.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_Gpio.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_I2C.obj: ../src/DSP2802x_I2C.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_I2C.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_MemCopy.obj: ../src/DSP2802x_MemCopy.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_MemCopy.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_OscComp.obj: ../src/DSP2802x_OscComp.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_OscComp.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_PieCtrl.obj: ../src/DSP2802x_PieCtrl.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_PieCtrl.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_PieVect.obj: ../src/DSP2802x_PieVect.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_PieVect.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_Sci.obj: ../src/DSP2802x_Sci.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_Sci.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_Spi.obj: ../src/DSP2802x_Spi.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_Spi.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_SysCtrl.obj: ../src/DSP2802x_SysCtrl.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_SysCtrl.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_TempSensorConv.obj: ../src/DSP2802x_TempSensorConv.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_TempSensorConv.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DSP2802x_usDelay.obj: ../src/DSP2802x_usDelay.asm $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv5/tools/compiler/c2000_6.2.0/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/ti/ccsv5/tools/compiler/c2000_6.2.0/include" --include_path="D:/Workspace/learnC2000" --include_path="D:/Workspace/learnC2000/inc" --include_path="D:/Workspace/learnC2000/src" -g --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="src/DSP2802x_usDelay.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '



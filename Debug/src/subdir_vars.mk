################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/DSP2802x_CodeStartBranch.asm \
../src/DSP2802x_DBGIER.asm \
../src/DSP2802x_DisInt.asm \
../src/DSP2802x_usDelay.asm 

C_SRCS += \
../src/DSP2802x_Adc.c \
../src/DSP2802x_Comp.c \
../src/DSP2802x_CpuTimers.c \
../src/DSP2802x_DefaultIsr.c \
../src/DSP2802x_ECap.c \
../src/DSP2802x_EPwm.c \
../src/DSP2802x_GlobalVariableDefs.c \
../src/DSP2802x_Gpio.c \
../src/DSP2802x_I2C.c \
../src/DSP2802x_MemCopy.c \
../src/DSP2802x_OscComp.c \
../src/DSP2802x_PieCtrl.c \
../src/DSP2802x_PieVect.c \
../src/DSP2802x_Sci.c \
../src/DSP2802x_Spi.c \
../src/DSP2802x_SysCtrl.c \
../src/DSP2802x_TempSensorConv.c 

OBJS += \
./src/DSP2802x_Adc.obj \
./src/DSP2802x_CodeStartBranch.obj \
./src/DSP2802x_Comp.obj \
./src/DSP2802x_CpuTimers.obj \
./src/DSP2802x_DBGIER.obj \
./src/DSP2802x_DefaultIsr.obj \
./src/DSP2802x_DisInt.obj \
./src/DSP2802x_ECap.obj \
./src/DSP2802x_EPwm.obj \
./src/DSP2802x_GlobalVariableDefs.obj \
./src/DSP2802x_Gpio.obj \
./src/DSP2802x_I2C.obj \
./src/DSP2802x_MemCopy.obj \
./src/DSP2802x_OscComp.obj \
./src/DSP2802x_PieCtrl.obj \
./src/DSP2802x_PieVect.obj \
./src/DSP2802x_Sci.obj \
./src/DSP2802x_Spi.obj \
./src/DSP2802x_SysCtrl.obj \
./src/DSP2802x_TempSensorConv.obj \
./src/DSP2802x_usDelay.obj 

ASM_DEPS += \
./src/DSP2802x_CodeStartBranch.pp \
./src/DSP2802x_DBGIER.pp \
./src/DSP2802x_DisInt.pp \
./src/DSP2802x_usDelay.pp 

C_DEPS += \
./src/DSP2802x_Adc.pp \
./src/DSP2802x_Comp.pp \
./src/DSP2802x_CpuTimers.pp \
./src/DSP2802x_DefaultIsr.pp \
./src/DSP2802x_ECap.pp \
./src/DSP2802x_EPwm.pp \
./src/DSP2802x_GlobalVariableDefs.pp \
./src/DSP2802x_Gpio.pp \
./src/DSP2802x_I2C.pp \
./src/DSP2802x_MemCopy.pp \
./src/DSP2802x_OscComp.pp \
./src/DSP2802x_PieCtrl.pp \
./src/DSP2802x_PieVect.pp \
./src/DSP2802x_Sci.pp \
./src/DSP2802x_Spi.pp \
./src/DSP2802x_SysCtrl.pp \
./src/DSP2802x_TempSensorConv.pp 

C_DEPS__QUOTED += \
"src\DSP2802x_Adc.pp" \
"src\DSP2802x_Comp.pp" \
"src\DSP2802x_CpuTimers.pp" \
"src\DSP2802x_DefaultIsr.pp" \
"src\DSP2802x_ECap.pp" \
"src\DSP2802x_EPwm.pp" \
"src\DSP2802x_GlobalVariableDefs.pp" \
"src\DSP2802x_Gpio.pp" \
"src\DSP2802x_I2C.pp" \
"src\DSP2802x_MemCopy.pp" \
"src\DSP2802x_OscComp.pp" \
"src\DSP2802x_PieCtrl.pp" \
"src\DSP2802x_PieVect.pp" \
"src\DSP2802x_Sci.pp" \
"src\DSP2802x_Spi.pp" \
"src\DSP2802x_SysCtrl.pp" \
"src\DSP2802x_TempSensorConv.pp" 

OBJS__QUOTED += \
"src\DSP2802x_Adc.obj" \
"src\DSP2802x_CodeStartBranch.obj" \
"src\DSP2802x_Comp.obj" \
"src\DSP2802x_CpuTimers.obj" \
"src\DSP2802x_DBGIER.obj" \
"src\DSP2802x_DefaultIsr.obj" \
"src\DSP2802x_DisInt.obj" \
"src\DSP2802x_ECap.obj" \
"src\DSP2802x_EPwm.obj" \
"src\DSP2802x_GlobalVariableDefs.obj" \
"src\DSP2802x_Gpio.obj" \
"src\DSP2802x_I2C.obj" \
"src\DSP2802x_MemCopy.obj" \
"src\DSP2802x_OscComp.obj" \
"src\DSP2802x_PieCtrl.obj" \
"src\DSP2802x_PieVect.obj" \
"src\DSP2802x_Sci.obj" \
"src\DSP2802x_Spi.obj" \
"src\DSP2802x_SysCtrl.obj" \
"src\DSP2802x_TempSensorConv.obj" \
"src\DSP2802x_usDelay.obj" 

ASM_DEPS__QUOTED += \
"src\DSP2802x_CodeStartBranch.pp" \
"src\DSP2802x_DBGIER.pp" \
"src\DSP2802x_DisInt.pp" \
"src\DSP2802x_usDelay.pp" 

C_SRCS__QUOTED += \
"../src/DSP2802x_Adc.c" \
"../src/DSP2802x_Comp.c" \
"../src/DSP2802x_CpuTimers.c" \
"../src/DSP2802x_DefaultIsr.c" \
"../src/DSP2802x_ECap.c" \
"../src/DSP2802x_EPwm.c" \
"../src/DSP2802x_GlobalVariableDefs.c" \
"../src/DSP2802x_Gpio.c" \
"../src/DSP2802x_I2C.c" \
"../src/DSP2802x_MemCopy.c" \
"../src/DSP2802x_OscComp.c" \
"../src/DSP2802x_PieCtrl.c" \
"../src/DSP2802x_PieVect.c" \
"../src/DSP2802x_Sci.c" \
"../src/DSP2802x_Spi.c" \
"../src/DSP2802x_SysCtrl.c" \
"../src/DSP2802x_TempSensorConv.c" 

ASM_SRCS__QUOTED += \
"../src/DSP2802x_CodeStartBranch.asm" \
"../src/DSP2802x_DBGIER.asm" \
"../src/DSP2802x_DisInt.asm" \
"../src/DSP2802x_usDelay.asm" 


